use strict;
use utf8;

use Encode;

mkdir("./utf8") unless -d "./utf8";

while(<STDIN>) {
    chomp;
    my $input = $_;
    if(-f $input) {
        open(my $infh, $input) or die;
        my @inbuf = <$infh>;
        close($infh);
        @inbuf = map {
            $_ =~ s/[\r\n]//g;
            encode('UTF-8', decode('cp932', $_));
        } @inbuf;
        open(my $outfh, ">", $input);
        print $outfh join("\n", @inbuf);
        close($outfh);
        print "$input: OK\n";
    } elsif(-d $input) {
        next;
    } else {
        exit;
    }
}